from django.conf.urls import url

from apps.events.views import EventListCreateAPIView

urlpatterns = [
    url(r'^/events$', EventListCreateAPIView.as_view(), name='event_list'),
]
