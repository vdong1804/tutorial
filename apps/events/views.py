from rest_framework.settings import api_settings

from apps.events.models import EventModel
from apps.events.serializers.event_serializers import EventSerializer
from commons.pagination import PaginationAPIView


class EventListCreateAPIView(PaginationAPIView):
    queryset = EventModel.objects.select_related('client').prefetch_related('event_authorized').filter(is_deleted=False)
    serializer_class = EventSerializer
    pagination_class = api_settings.DEFAULT_PAGINATION_CLASS  # cool trick right? :)

    # We need to override get method to achieve pagination
    def get(self, request):
        page = self.paginate_queryset(self.queryset)
        if page is not None:
            serializer = self.serializer_class(page, many=True)
            return self.get_paginated_response(serializer.data)
