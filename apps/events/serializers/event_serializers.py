from rest_framework import serializers

from apps.events.models import EventModel


class EventSerializer(serializers.ModelSerializer):
    is_locked = serializers.SerializerMethodField()
    image_url = serializers.SerializerMethodField()

    class Meta:
        model = EventModel
        fields = ['id', 'title', 'is_locked', 'image_url']

    # custom response trả về cho field is_locked
    def get_is_locked(self, obj):
        is_locked = 0
        if obj.is_private and len(obj.event_authorized.all()) == 0:
            is_locked = 1

        return is_locked

    # custom response trả về cho field image_url
    def get_image_url(self, obj):
        # image_path = obj.image_path.first()
        image_path = obj.image_path.all() # get all dữ liệu image path theo event
        image_path_list = list(image_path)
        if len(image_path_list) > 0:
            image_path = image_path_list[0]
            return image_path.image_url
        return ''
