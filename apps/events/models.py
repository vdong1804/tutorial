from django.db import models

from apps.users.models import ClientModel, UserAccountModel
from commons.models import BaseModel


class EventModel(BaseModel):
    client = models.ForeignKey(ClientModel, on_delete=models.CASCADE, null=True, related_name='events',
                               verbose_name='Khách hàng')
    title = models.CharField(max_length=255, null=False, blank=False, verbose_name='Tiêu đề')
    body = models.TextField(max_length=255, null=True, blank=True, verbose_name='Nội dung')
    is_private = models.BooleanField(default=False, choices=((0, 'No'), (1, 'Yes')))

    class Meta:
        db_table = 'events'
        verbose_name_plural = 'Sự kiện'


class EventAuthorizedModel(BaseModel):
    event = models.ForeignKey(EventModel, on_delete=models.CASCADE, null=True, related_name='event_authorized')
    user = models.ForeignKey(UserAccountModel, on_delete=models.CASCADE, null=True, related_name='event_authorized')

    class Meta:
        db_table = 'event_authorized'


class ImagePathModel(BaseModel):
    event = models.ForeignKey(EventModel, on_delete=models.CASCADE, null=True, related_name='image_path')
    user = models.ForeignKey(UserAccountModel, on_delete=models.CASCADE, null=True, related_name='image_path')
    image_url = models.CharField(max_length=255, null=True)

    class Meta:
        db_table = 'image_path'
